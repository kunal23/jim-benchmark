package com.appnomic;

import com.appnomic.ds.Trie;
import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.infra.Blackhole;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

@State(Scope.Benchmark)
public class TrieRegexBenchmarks {

    Trie trie;
    List<Pattern> patternList;
    String[] patterns = {
            "car",
            "cat",
            "cab",
            "java",
            "com.example.pkg.Class.method",
            "java.net.SocketInputStream.read()",
            "java.net.SocketOutputStream.write()"
    };
    String[] keys = {
            "java.net.SocketInputStream.read()",
            "java.net.SocketOutputStream.write()",
//            "java.net",
//            "java.net.SocketInputStream",
//            "java.net.SocketOutputStream",
//            "java.net.SocketInputStream.read().ignore",
//            "java.net.SocketOutputStream.write().ignore"
    };

    @Setup
    public void setup() {
        trie = new Trie();
        patternList = new ArrayList<>();
        for (String s : patterns) {
            trie.add(s);
            patternList.add(Pattern.compile(s));
        }
    }

    @Benchmark
    @BenchmarkMode(Mode.AverageTime)
    @OutputTimeUnit(TimeUnit.NANOSECONDS)
    public void trieMatch(Blackhole bh) {
        for (String key : keys) {
            bh.consume(trie.contains(key));
        }
    }

    @Benchmark
    @BenchmarkMode(Mode.AverageTime)
    @OutputTimeUnit(TimeUnit.NANOSECONDS)
    public void regexMatch(Blackhole bh) {
        for (String key : keys) {
            for (Pattern p : patternList) bh.consume(p.matcher(key).matches());
        }
    }



    public static void main(String[] args) throws RunnerException {
        Options opt = new OptionsBuilder()
                .include(TrieRegexBenchmarks.class.getSimpleName())
                .forks(1)
                .warmupIterations(10)
                .measurementIterations(10)
                .build();

        new Runner(opt).run();
    }

}
