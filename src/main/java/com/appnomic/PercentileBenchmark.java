package com.appnomic;

import com.appnomic.quickselect.QuickSelect;
import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.infra.Blackhole;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;
import org.openjdk.jol.info.GraphLayout;

import java.util.Arrays;
import java.util.Random;
import java.util.concurrent.TimeUnit;

@State(Scope.Benchmark)
@BenchmarkMode(Mode.AverageTime)
@OutputTimeUnit(TimeUnit.MICROSECONDS)
public class PercentileBenchmark {

    static long[] hundred               = new long[100];
    static long[] thousand              = new long[1000];
    static long[] tenThousand           = new long[10000];
    static long[] hundredThousand       = new long[100000];
    static long[] million               = new long[1000000];
    static long[] tenMillion            = new long[10000000];

    int getNormRank(int size) {
        return Math.round((95 / 100) * size);
    }

    static {
        fill(hundred);
        fill(thousand);
        fill(tenThousand);
        fill(hundredThousand);
        fill(million);
        fill(tenMillion);
    }

    private static void fill(long[] arr) {
        Random random = new Random();
        for (int i= 0; i < arr.length; i++) arr[i] = random.nextLong();
    }

    @Benchmark
    public void sortingHundred(Blackhole bh) {
        Arrays.sort(hundred);
        bh.consume(hundred[getNormRank(hundred.length)]);
    }

    @Benchmark
    public void sortingThousand(Blackhole bh) {
        Arrays.sort(thousand);
        bh.consume(thousand[getNormRank(thousand.length)]);
    }

    @Benchmark
    public void sortingTenThousand(Blackhole bh) {
        Arrays.sort(tenThousand);
        bh.consume(tenThousand[getNormRank(tenThousand.length)]);
    }

    @Benchmark
    public void sortingHundredThousand(Blackhole bh) {
        Arrays.sort(hundredThousand);
        bh.consume(hundredThousand[getNormRank(hundredThousand.length)]);
    }

    @Benchmark
    public void sortingMillion(Blackhole bh) {
        Arrays.sort(million);
        bh.consume(million[getNormRank(million.length)]);
    }

    @Benchmark
    public void sortingTenMillion(Blackhole bh) {
        Arrays.sort(tenMillion);
        bh.consume(tenMillion[getNormRank(tenMillion.length)]);
    }

    @Benchmark
    public void quickSelectHundred(Blackhole bh) {
        bh.consume(QuickSelect.selectIterative(hundred, getNormRank(hundred.length)));
    }

    @Benchmark
    public void quickSelectThousand(Blackhole bh) {
        bh.consume(QuickSelect.selectIterative(thousand, getNormRank(thousand.length)));
    }

    @Benchmark
    public void quickSelectTenThousand(Blackhole bh) {
        bh.consume(QuickSelect.selectIterative(tenThousand, getNormRank(tenThousand.length)));
    }

    @Benchmark
    public void quickSelectHundredThousand(Blackhole bh) {
        bh.consume(QuickSelect.selectIterative(hundredThousand, getNormRank(hundredThousand.length)));
    }

    @Benchmark
    public void quickSelectMillion(Blackhole bh) {
        bh.consume(QuickSelect.selectIterative(million, getNormRank(million.length)));
    }

    @Benchmark
    public void quickSelectTenMillion(Blackhole bh) {
        bh.consume(QuickSelect.selectIterative(tenMillion, getNormRank(tenMillion.length)));
    }

    public static void main(String[] args) throws RunnerException {
        Options opt = new OptionsBuilder()
                .include(PercentileBenchmark.class.getSimpleName())
                .forks(1)
                .warmupIterations(10)
                .measurementIterations(10)
                .build();

//        new Runner(opt).run();

        System.err.println("*******Sizes******");
        System.err.println("size of hundred response times          : " + size(hundred) + " kb.");
        System.err.println("size of thousand response times         : " + size(thousand) + " kb.");
        System.err.println("size of ten thousand response times     : " + size(tenThousand) + " kb.");
        System.err.println("size of hundred thousand response times : " + size(hundredThousand) + " kb.");
        System.err.println("size of million response times          : " + size(million) + " kb.");
        System.err.println("size of ten million response times      : " + size(tenMillion) + " kb.");
    }

    private static double size(long[] arr) {
        return ((double)GraphLayout.parseInstance(arr).totalSize()) / 1024;
    }

}
