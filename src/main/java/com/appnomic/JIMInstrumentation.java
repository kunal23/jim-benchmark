/*
 * Copyright (c) 2014, Oracle America, Inc.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  * Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 *  * Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 *  * Neither the name of Oracle nor the names of its contributors may be used
 *    to endorse or promote products derived from this software without
 *    specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */

package com.appnomic;

import com.appnomic.appsone.jia.agent.InvokeRegistry;
import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

import java.util.concurrent.TimeUnit;

@State(Scope.Benchmark)
public class JIMInstrumentation {

    @Benchmark
    @BenchmarkMode(Mode.AverageTime)
    @OutputTimeUnit(TimeUnit.NANOSECONDS)
    public void avgtInvokeRegistry() {
        instrumentIBMMQ();
    }

    @Benchmark
    @BenchmarkMode(Mode.Throughput)
    @OutputTimeUnit(TimeUnit.SECONDS)
    public void thrptInvokeRegistry() {
        instrumentIBMMQ();
    }

//    @Benchmark
//    @BenchmarkMode(Mode.SampleTime)
//    @OutputTimeUnit(TimeUnit.NANOSECONDS)
//    public void sampleInvokeRegistry() {
//        instrumentIBMMQ();
//    }


    private void instrumentIBMMQ() {
        InvokeRegistry.invokeData("com.ibm.mq.MQClientManagedConnectionFactoryJ11._createManagedConnection(javax.resource.spi.ConnectionRequestInfo,boolean)",
                "GET",
                "http://someurl",
                "somedata",
                1,
                100,
                100,
                "ENTRY",
                "IBM_MQ",
                0,
                1,
                "http://someurl",
                "DATA",
                false,
                false,
                null
        );

        InvokeRegistry.invokeData("com.ibm.mq.MQClientManagedConnectionFactoryJ11._createManagedConnection(javax.resource.spi.ConnectionRequestInfo,boolean)",
                "GET",
                "http://someurl",
                "somedata",
                1,
                100,
                100,
                "EXIT",
                "IBM_MQ",
                0,
                1,
                "http://someurl",
                "DATA",
                false,
                false,
                null
        );

        InvokeRegistry.invokeData("com.ibm.mq.MQClientManagedConnectionFactoryJ11._createManagedConnection(javax.resource.spi.ConnectionRequestInfo,boolean)",
                "GET",
                "http://someurl",
                "somedata",
                1,
                100,
                200,
                "EXIT",
                "IBM_MQ",
                1,
                1,
                null,
                "DATA",
                false,
                false,
                null
        );

        InvokeRegistry.invokeData("com.ibm.mq.MQClientManagedConnectionFactoryJ11._createManagedConnection(javax.resource.spi.ConnectionRequestInfo,boolean)",
                "GET",
                "http://someurl",
                "somedata",
                1,
                100,
                200,
                "ENTRY",
                "IBM_MQ",
                1,
                1,
                null,
                "DATA",
                false,
                false,
                null
        );
    }


    public static void main(String[] args) throws RunnerException {
        Options opt = new OptionsBuilder()
                .include(JIMInstrumentation.class.getSimpleName())
                .forks(1)
                .warmupIterations(10)
                .measurementIterations(10)
                .build();

        new Runner(opt).run();
    }
}
