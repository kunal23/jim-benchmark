package com.appnomic;

import com.yahoo.sketches.quantiles.DoublesSketch;
import com.yahoo.sketches.quantiles.UpdateDoublesSketch;
import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.infra.Blackhole;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;
import org.openjdk.jol.info.GraphLayout;

import java.util.concurrent.TimeUnit;

@State(Scope.Benchmark)
public class DataSketchBenchMarks {

    private final static int LIMIT = 100000000;

    UpdateDoublesSketch updateSketch;
    int[] arr = new int[LIMIT];
    int idx = 0;
    double[] normRanks = {0, 0.5, 0.85, 0.9, 0.95, 1.0};

    @Setup(Level.Trial)
    public void setup() {
        updateSketch = DoublesSketch.builder().setK(512).build();
        for (int i = 0; i < LIMIT; i++) {
//            arr[i] = i;
            updateSketch.update(i);
        }
    }

//    @Benchmark
//    @BenchmarkMode(Mode.Throughput)
//    public void updateThrpt() {
//        method();
//    }
//
//    @Benchmark
//    @BenchmarkMode(Mode.AverageTime)
//    @OutputTimeUnit(TimeUnit.NANOSECONDS)
//    public void updateAvgt() {
//        method();
//    }
//
//    private void method() {
//        if (idx == LIMIT) idx = 0;
//        updateSketch.update(arr[idx++]);
//    }

    @Benchmark
    @BenchmarkMode(Mode.AverageTime)
    @OutputTimeUnit(TimeUnit.NANOSECONDS)
    public void percentilesAvgt(Blackhole bh) {
        bh.consume(updateSketch.getQuantiles(normRanks));
    }

    @Benchmark
    @BenchmarkMode(Mode.Throughput)
    @OutputTimeUnit(TimeUnit.SECONDS)
    public void percentilesThpt(Blackhole bh) {
        bh.consume(updateSketch.getQuantiles(normRanks));
    }


    public static void main(String[] args) throws RunnerException {
//        Options opt = new OptionsBuilder()
//                .include(DataSketchBenchMarks.class.getSimpleName())
//                .forks(1)
//                .warmupIterations(10)
//                .measurementIterations(10)
//                .build();
//
//        new Runner(opt).run();
//
//        UpdateDoublesSketch updateSketch = DoublesSketch.builder().setK(512).build();
//        for (int i = 0; i < Integer.MAX_VALUE - 1; i++) {
////            arr[i] = i;
//            updateSketch.update(i);
//        }
//
//        System.err.println(GraphLayout.parseInstance(updateSketch).totalSize());
        //(err)(?=(?:[^"]*"[^"]*")*[^"]*$)
    }
}
